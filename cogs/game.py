import discord
from discord.ext import commands


class Game:
    game = None

    def __init__(self, client):
        self.client = client

    @commands.command(
        name='load_game',
        description='Charge un jeu'
    )
    async def load_game(self, game):
        self.game = game
        try:
            self.client.load_extension(game)
            print('{} loaded'.format(game))
        except Exception as error:
            self.game = None
            print('{} cannot be loaded, [{}]'.format(game, error))

    @commands.command(
        name='end_game',
        description='Quitte un jeu'
    )
    async def end_game(self):
        await self.client.change_presence(game=discord.Game(name='se toucher les couilles'))
        try:
            self.client.unload_extension(self.game)
            print('{} unloaded'.format(self.game))
            self.game = None
        except Exception as error:
            print('{} cannot be unloaded, [{}]'.format(self.game, error))


def setup(client):
    client.add_cog(Game(client))
