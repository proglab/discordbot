import discord
from discord.ext import commands
from datetime import datetime as d


class Basics:
    def __init__(self, client):
        self.client = client

    @commands.command(
        name='ping',
        description='Une commande de ping'
    )
    async def ping(self):
        start = d.timestamp(d.now())
        await self.client.say('Pong!')
        time = int((d.timestamp(d.now()) - start) * 1000)
        await self.client.say('Un message prend {}ms.'.format(time))


def setup(client):
    client.add_cog(Basics(client))