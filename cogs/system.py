import discord
from discord.ext import commands
import os
import sys
import psutil


class System:
    def __init__(self, client):
        self.client = client

    @commands.command(
        name='restart',
        description='Restart the script'
    )
    async def restart(self):
        await self.client.say('Restarting the script...')
        try:
            p = psutil.Process(os.getpid())
            for handler in p.open_files() + p.connections():
                os.close(handler.fd)
        except Exception as e:
            print(e)

        os.execl(sys.executable, sys.executable, *sys.argv)

    @commands.command(
        name='status',
        description='Status du raspberry'
    )
    async def status(self):
        time = os.popen('uptime -p').readline().replace('\n', '').replace('up ', '').replace(',', '')
        temp = os.popen('vcgencmd measure_temp').readline().replace('temp=', '')
        await self.client.say('up for %s, temp is %s' % (time, temp))

    @commands.command(
        name='reboot',
        description='Reboot du raspberry',
        aliases=['restart_pi', 'reboot_pi']
    )
    async def reboot(self):
        await self.client.say('Rebooting...')
        os.system("sudo shutdown -r now")

    @commands.command(
        name='shutdown',
        description='Shutdown du raspberry',
        aliases=['shutdown_pi']
    )
    async def shutdown(self):
        await self.client.say('Shutting down.')
        os.system("sudo shutdown -h now")


def setup(client):
    client.add_cog(System(client))