import discord
from discord.ext import commands
from abc import ABCMeta, abstractmethod


class PortAuxSinges:
    client = None
    in_game = False
    lieu = None
    inventaire = []
    pieces = {}

    def __init__(self, client):
        self.client = client
        self.pieces['bateau'] = Bateau(self)
        self.pieces['quartier'] = Quartier(self)
        self.pieces['ponton'] = Ponton(self)
        self.pieces['place'] = Place(self)
        self.pieces['palais'] = Palais(self)
        self.pieces['auberge'] = Auberge(self)
        self.pieces['jardins'] = Jardins(self)

    @commands.command(
        name='start_game',
        description='Démarre le jeu',
        pass_context=True
    )
    async def start_game(self, ctx):
        if not self.in_game:
            self.in_game = True
            await self.client.change_presence(game=discord.Game(name='Port aux singes'))
            await self.pieces['quartier'].enter(ctx.message.channel)

    async def on_message(self, message):
        if message.content is not None and message.content.startswith(self.client.command_prefix):
            await self.client.process_commands(message)
            return

        if message.author != self.client.user:
            await self.lieu.do_action(message)


class Piece(metaclass=ABCMeta):
    actions = {}
    marie = False
    fleurs = False
    molosse = True

    def __init__(self, game):
        self.game = game
        self.sorties = []

    def get_sorties(self):
        s = ''
        for sortie in self.sorties:
            s = s + '- ' + sortie + '\n'
        return s

    def get_inventaire(self):
        if len(self.game.inventaire) > 0 :
            i = ''
            for inv in self.game.inventaire:
                i = i + '- ' + inv + '\n'
        else:
            i = 'Vide'
        return i

    async def enter(self, channel):
        self.game.lieu = self
        print('Entrez dans la pièce : {}'.format(self.title))
        embed = discord.Embed(
            title=self.title,
            description=self.description,
            color=discord.Color.orange()
        )

        embed.set_footer(text='Château Aventure - Port aux singes')
        # embed.set_image()
        # embed.set_thumbnail()
        embed.set_author(name=self.lieu)
        embed.add_field(name='Les sorties sont :', value=self.get_sorties(), inline=True)
        embed.add_field(name='Inventaire :', value=self.get_inventaire(), inline=True)
        await self.game.client.send_message(channel, embed=embed)

    async def no_action(self, message):
        embed = discord.Embed(
            title='Je ne comprends pas cette commande : {}'.format(message.content),
            color=discord.Color.orange()
        )
        await self.game.client.send_message(message.channel, embed=embed)

    async def send_embed_action(self, message, channel):
        embed = discord.Embed(
            description='{}'.format(message),
            color=discord.Color.orange()
        )
        embed.add_field(name='Inventaire :', value=self.get_inventaire(), inline=True)
        await self.game.client.send_message(channel, embed=embed)

    async def do_action(self, message):
        self.init_action()
        print('do_action : {}'.format(message.content))
        for action in self.actions:
            print('action disponible : {}'.format(action))
        if message.content in self.actions:
            if message.content.startswith('Aller à '):
                await self.actions[message.content].enter(message.channel)
            else:
                await self.actions[message.content](message)
        else:
            await self.no_action(message)

    def init_action(self):
        self.actions.clear()
        if 'Feuille' in self.game.inventaire:
            self.actions['Lire Feuille'] = self.lire_feuille

    async def lire_feuille(self, message):
        if 'Ruban' not in self.game.inventaire:
            self.game.inventaire.append('Ruban')
            await self.send_embed_action('Vous déroulez la ***feuille*** en prenant soin de dénouer le ***ruban*** qui se trouve dessus. Vous lisez : "Tâche importante à réaliser : voler la ***carte aux trésors***". Le ***ruban*** est dans votre inventaire.', message.channel)
        else:
            await self.send_embed_action('Vous lisez : "Tâche importante à réaliser : voler la ***carte aux trésors***".', message.channel)


class Quartier(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Quartier du capitaine'
        self.sorties = ["Aller à Bateau"]
        self.title = 'Vous êtes dans vos quartiers.'
        self.description = 'Il y a un lit simple et un bureau digne des plus grands pirates. Une feuille enroulée se trouve sur votre bureau.'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Bateau'] = self.game.pieces['bateau']
        self.actions['Fouiller lit'] = self.fouiller_lit
        self.actions['Fouiller bureau'] = self.fouiller_bureau
        if 'Feuille' not in self.game.inventaire:
            self.actions['Prendre feuille'] = self.prendre_feuille

    async def fouiller_lit(self, message):
        if 'Deux doublons' not in self.game.inventaire:
            self.game.inventaire.append('Deux doublons')
            await self.send_embed_action('Vous trouvez ***deux doublons***. Vous avez maintenant ***deux doublons*** dans votre inventaire.', message.channel)
        else:
            await self.send_embed_action('Il n\'y a rien dedans.', message.channel)

    async def fouiller_bureau(self, message):
        if 'Feuille' not in self.game.inventaire:
            await self.send_embed_action('Il n’y a rien d’autre que la ***feuille*** enroulée', message.channel)
        else:
            await self.send_embed_action('Il n\'y a rien dedans.', message.channel)

    async def prendre_feuille(self, message):
        self.game.inventaire.append('Feuille')
        await self.send_embed_action('La ***feuille*** est dans votre inventaire.', message.channel)


class Bateau(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Bateau'
        self.sorties = ["Aller à Vos quartier", "Aller à Le ponton"]
        self.title = 'Vous êtes sur le pont de la Bellissima, votre splendide bateau de pirate.'
        self.description = 'Votre mousse se tient là près à obéir à vos ordres.'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Vos quartier'] = self.game.pieces['quartier']
        self.actions['Aller à Le ponton'] = self.game.pieces['ponton']
        self.actions['Parler mousse'] = self.parler_mousse
        self.actions['Demander Mousse acheter somnifère'] = self.demander_mousse

    async def parler_mousse(self, message):
        await self.send_embed_action('Il a l’air très très fatigué. *"Capitaine, on compte sur vous !"*', message.channel)

    async def demander_mousse(self, message):
        if 'Deux doublons' not in self.game.inventaire and 'Un doublon' not in self.game.inventaire:
            await self.send_embed_action('Vous n\'avez pas de doublon.', message.channel)
        if 'Deux doublons' in self.game.inventaire:
            self.game.inventaire.remove('Deux doublons')
            self.game.inventaire.append('Un doublon')
            self.game.inventaire.append('Somnifère')
            await self.send_embed_action('Le mousse récupère un puissant ***somnifère*** contre ***un doublon***, vous avez un ***somnifère*** dans votre inventaire.', message.channel)
        if 'Un doublon' in self.game.inventaire:
            self.game.inventaire.remove('Un doublon')
            self.game.inventaire.append('Somnifère')
            await self.send_embed_action('Le mousse récupère un puissant ***somnifère*** contre ***un doublon***, vous avez un ***somnifère*** dans votre inventaire.', message.channel)


class Ponton(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Le ponton'
        self.sorties = ["Aller à Bateau", "Aller à Place centrale"]
        self.title = 'Vous êtes sur le ponton de Port aux Singes'
        self.description = 'Devant vous s’étend la cité portuaire de port aux Singes. Votre second se trouve là près de vous'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Bateau'] = self.game.pieces['bateau']
        self.actions['Aller à Place centrale'] = self.game.pieces['place']
        self.actions['Parler Second'] = self.parler_second

    async def parler_second(self, message):
        await self.send_embed_action('*"Capitaine, la révolte gronde. Si vous n’arrivez pas à vous emparer de la ***carte aux trésors*** du gouverneur, vous risquez la mutinerie."*', message.channel)


class Place(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Place de la ville'
        self.sorties = ["Aller à Le ponton", "Aller à L\'auberge", "Aller à Le palais du gouverneur", "Aller à Les jardins privés"]
        self.title = 'Vous êtes sur la grande place de Port aux Singes'
        if not self.marie:
            self.description = 'Un colporteur vend toute sorte de choses. Une estrade se trouve là avec une affiche.'
        else:
            self.description = 'Une estrade se trouve là avec une affiche.'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Le ponton'] = self.game.pieces['ponton']
        self.actions['Aller à L\'auberge'] = self.game.pieces['auberge']
        self.actions['Aller à Le palais du gouverneur'] = self.game.pieces['palais']
        self.actions['Aller à Les jardins privés'] = self.game.pieces['jardins']
        self.actions['Lire affiche'] = self.lire_affiche
        self.actions['Parler colporteur'] = self.parler_colporteur
        self.actions['Acheter somnifère'] = self.acheter_somnifere

    async def lire_affiche(self, message):
        await self.send_embed_action('Apparemment vous avez raté un concert de la célèbre Maria Carabas. Il avait lieu hier', message.channel)

    async def parler_colporteur(self, message):
        await self.send_embed_action('Potions potions potions potions potions…', message.channel)

    async def acheter_somnifere(self, message):
        if 'Deux doublons' not in self.game.inventaire and 'Un doublon' not in self.game.inventaire:
            await self.send_embed_action('Vous n\'avez pas de doublon.', message.channel)
            return
        if 'Un doublon' in self.game.inventaire:
            self.game.inventaire.remove('Un doublon')
            self.game.inventaire.append('Somnifère')
            await self.send_embed_action('Vous récupèrez un puissant ***somnifère*** contre ***un doublon***, vous avez un ***somnifère*** dans votre inventaire.', message.channel)
            return
        if 'Deux doublons' in self.game.inventaire:
            self.game.inventaire.remove('Deux doublons')
            self.game.inventaire.append('Un doublon')
            self.game.inventaire.append('Somnifère')
            await self.send_embed_action('Vous récupèrez un puissant ***somnifère*** contre ***un doublon***, vous avez un ***somnifère*** dans votre inventaire.', message.channel)


class Auberge(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Auberge'
        self.sorties = ["Aller à Place centrale"]
        self.title = 'Vous êtes dans l’auberge du cygne noir'
        self.description = 'L’aubergiste qui se tient là semble heureux. Parmi les clients vous voyez Maria Carabas, la célèbre cantatrice.'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Place centrale'] = self.game.pieces['place']
        self.actions['Parler à Maria'] = self.parler_maria
        self.actions['Parler à l\'aubergiste'] = self.parler_aubergiste
        self.actions['Parler aubergiste'] = self.parler_aubergiste
        self.actions['Acheter nourriture aubergiste'] = self.acheter_nourriture
        if 'Cuisse de poulet' in self.game.inventaire:
            self.actions['Manger Cuisse de poulet'] = self.manger_poulet
        if 'Cuisse de poulet' in self.game.inventaire and 'Somnifère' in self.game.inventaire:
            self.actions['Combiner cuisse de poulet avec somnifère'] = self.combiner_cuisse_somnifere
        if self.fleurs:
            self.actions['Demander Maria de chanter'] = self.maria_chanter

    async def parler_maria(self, message):
        if 'bouquet de fleur' not in self.game.inventaire and 'fleurs' not in self.game.inventaire:
            await self.send_embed_action('Son agent s’interpose : *"Ne dérangez pas la célèbre cantatrice !"*.', message.channel)
        if 'fleurs' in self.game.inventaire:
            await self.send_embed_action('L’agent s’interpose : *"Allons Maria mérite quelque chose de plus distingué que quelques fleurs tenues dans une main."*', message.channel)
        if 'bouquet de fleur' in self.game.inventaire:
            self.fleurs = True
            await self.send_embed_action('*"Oh ! Quelles belles fleurs ! Que puis-je faire pour vous ?"*', message.channel)

    async def parler_aubergiste(self, message):
        await self.send_embed_action('*"Que puis-je pour vous ?"*', message.channel)

    async def acheter_nourriture(self, message):
        if 'Deux doublons' not in self.game.inventaire and 'Un doublon' not in self.game.inventaire:
            await self.send_embed_action('Vous n\'avez pas de doublon.', message.channel)
        if 'Deux doublons' in self.game.inventaire:
            self.game.inventaire.remove('Deux doublons')
            self.game.inventaire.append('Un doublon')
            self.game.inventaire.append('Cuisse de poulet')
            await self.send_embed_action('Celui-ci vous apporte une ***cuisse de poulet*** pour un doublon.', message.channel)
        if 'Un doublon' in self.game.inventaire:
            self.game.inventaire.remove('Un doublon')
            self.game.inventaire.append('Cuisse de poulet')
            await self.send_embed_action('Celui-ci vous apporte une ***cuisse de poulet*** pour un doublon.', message.channel)

    async def manger_poulet(self, message):
        self.game.inventaire.remove('Cuisse de poulet')
        await self.send_embed_action('Vous mangez la ***cuisse de poulet.***', message.channel)

    async def combiner_cuisse_somnifere(self, message):
        self.game.inventaire.remove('Cuisse de poulet')
        self.game.inventaire.remove('Somnifère')
        self.game.inventaire.append('Cuisse de poulet empoisonnée')
        await self.send_embed_action('Vous avez maintenant une ***cuisse de poulet empoisonnée*** dans votre inventaire', message.channel)

    async def maria_chanter(self, message):
        await self.send_embed_action('Toute l’île aux singes vient écouter la chanteuse, vous apercevez dans la foule votre second, votre mousse, le colporteur, Ricko le borgne, Blaise le Pascal, Pat le Bulaire… et les gardes du palais du gouverneur', message.channel)


class Palais(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Palais du Gouverneur'
        self.sorties = ["Aller à Place centrale"]
        self.title = 'Vous êtes dans le Palais du Gouverneur'
        if self.marie:
            self.description = 'Vous arrivez devant le présentoir sur lequel se trouve la ***carte au trésor*** tant convoité.'
        else:
            self.description = 'Des gardes fortement armés gardent un présentoir sur lequel se trouve la ***carte au trésor*** tant convoitée.'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Place centrale'] = self.game.pieces['place']
        self.actions['Prendre carte au trésor'] = self.prendre_carte

    async def prendre_carte(self, message):
        if self.marie:
            await self.send_embed_action('Vous avez gagné', message.channel)
        else:
            await self.send_embed_action('Les gardes vous empêche de vous approcher de la ***carte au trésor*** tant convoitée.', message.channel)


class Jardins(Piece):
    def __init__(self, game):
        Piece.__init__(self, game)
        self.lieu = 'Jardins privés'
        self.sorties = ["Aller à Place centrale"]
        self.title = 'Vous êtes près de l’entrée du Jardins privés'
        self.description = 'Un énorme molosse barre l’entrée du jardin privé, rempli de fleurs, plus belles les unes que les autres.'

    def init_action(self):
        Piece.init_action(self)
        self.actions['Aller à Place centrale'] = self.game.pieces['place']
        self.actions['Donner cuisse empoisonnée au molosse'] = self.donner_cuisse_empoisonnee
        self.actions['Cueillir fleurs'] = self.cueillir_fleur

    async def donner_cuisse_empoisonnee(self, message):
        self.molosse = False
        await self.send_embed_action('Le molosse mange goulûment la cuisse empoisonnée et s\'endort immédiatement. Ce somnifère est vraiment très puissant.', message.channel)

    async def cueillir_fleur(self, message):
        if self.molosse:
            await self.send_embed_action('Vous vous approchez pour ceuillir des ***fleurs*** mais le molosse grogne et risque de vous mordre. Vous décidez donc de reculer.', message.channel)
        else:
            self.game.inventaire.append('Fleurs')
            await self.send_embed_action('Vous cueillez plein de ***fleurs***. Vous avez maintenant pleins de ***fleurs*** dans votre inventaire.', message.channel)


def setup(client):
    client.add_cog(PortAuxSinges(client))